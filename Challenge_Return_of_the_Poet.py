import tkinter as tk
from tkinter.filedialog import asksaveasfilename
from tkinter import messagebox
import random

# to make the window responsive
def resposive():
    for i in range(window.grid_size()[0]):  # for all columns
        window.grid_columnconfigure(i, weight=1)
    for i in range(window.grid_size()[1]):  # for all rows
        window.grid_rowconfigure(i, weight=1)

# to validate input lists
def process_input(*args):
    for arg in args:
        if not isinstance(arg, list):
            return False
        if len(arg) < 3:
            return False
        if not all(isinstance(item, str) for item in arg):
            return False
    return True

# to save generated poem to a file
def save_file():
    filepath = asksaveasfilename(
    defaultextension="txt",
    filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")],
    )

    if not filepath:
        return

    with open(filepath, "w") as output_file:
        output_file.write(generated_poem)

    window.title(f"Challenge_Return_of_the_Poet - {filepath}")

# Function to generate a poem
def make_poem():
    global generated_poem
    nouns_input = txt_edit_Nouns.get("1.0", tk.END).strip()
    verbs_input = txt_edit_Verbs.get("1.0", tk.END).strip()
    adjectives_input = txt_edit_Adjectives.get("1.0", tk.END).strip()
    prepositions_input = txt_edit_Prepositions.get("1.0", tk.END).strip()
    adverbs_input = txt_edit_Adverbs.get("1.0", tk.END).strip()

    # remove duplicates from input
    Nouns = list(set(nouns_input.split(',')))
    Verbs = list(set(verbs_input.split(',')))
    Adjectives = list(set(adjectives_input.split(',')))
    Prepositions = list(set(prepositions_input.split(',')))
    Adverbs = list(set(adverbs_input.split(',')))

    # validate input 
    if not process_input(Nouns, Adjectives, Verbs, Prepositions):
        messagebox.showerror("Error", "Please put at least 3 words. Remember, they must be different")
        return

    if len(Adverbs) < 1:
        messagebox.showerror("Error", "Please put at least 1 word")
        return
    
    # random words from lists 
    n1, n2, n3 = random.sample(Nouns, 3)
    v1, v2, v3 = random.sample(Verbs, 3)
    adj1, adj2, adj3 = random.sample(Adjectives, 3)
    prep1, prep2 = random.sample(Prepositions, 2)
    adv1 = random.choice(Adverbs)

    poem = f"{adj1} {n1}\n\n A {adj1} {n1} {v1} {prep1} the {adj2} {n2}\n{adv1}, the {n1} {v2}\nthe {n2} {v3} {prep2} a {adj3} {n3}"
    print(poem)
    
    poema = tk.Label(window, text=poem)
    poema.grid(row=12, column=1, sticky="nsew")
    
    generated_poem = poem

    return poem

# create groove frame
def create_groove_frame(parent_frame, start_row = 9, num_rows = 7, start_column = 0, num_columns = 7):
    groove_frame = tk.Frame(parent_frame, bd=2, relief="groove")
    groove_frame.grid(row=start_row, column=start_column, rowspan=num_rows, columnspan=num_columns, sticky="nsew")
    parent_frame.grid_columnconfigure(start_column, weight=1)  
    parent_frame.grid_rowconfigure(list(range(start_row, start_row + num_rows)), weight=1)  
    for i in range(num_rows):
        for j in range(num_columns):
            filler = tk.Label(groove_frame,)
            filler.grid(row=i, column=j, sticky="nsew")

# global variable to store poem
generated_poem = ""

window = tk.Tk()
window.title("Challenge_Return_of_the_Poet")

# Text inputs 
# Labels for input categories
labels_texts = ["Nouns", "Verbs", "Adjectives", "Prepositions", "Adverbs"]
for i, text in enumerate(labels_texts, start=1):
    label = tk.Label(window, text=text)
    label.grid(row=i, column=0, sticky="sen")

label_Inside_title = tk.Label(text = f"Enter your favorite words(at least 3 of each and 1 Adverb ), separated by commas")
label_Inside_title.grid(row=0,column=1, sticky="new")

txt_edit_Nouns = tk.Text(window, height=1)
txt_edit_Nouns.grid(row=1, column=1, sticky="ew", padx=5, pady=5)
txt_edit_Verbs = tk.Text(window, height=1)
txt_edit_Verbs.grid(row=2, column=1, sticky="ew", padx=5, pady=5)
txt_edit_Adjectives = tk.Text(window, height=1)
txt_edit_Adjectives.grid(row=3, column=1, sticky="ew", padx=5, pady=5)
txt_edit_Prepositions = tk.Text(window, height=1)
txt_edit_Prepositions.grid(row=4, column=1, sticky="ew", padx=5, pady=5)
txt_edit_Adverbs = tk.Text(window, height=1)
txt_edit_Adverbs.grid(row=5, column=1, sticky="ew", padx=5, pady=5)

create_groove_frame(window)

label_Inside_frame = tk.Label(text = f"My poem:")
label_Inside_frame.grid(row=11,column=1, sticky="new")

# Generate button
gen_button_fr = tk.Frame(window)
gen_button_fr.grid(row=6, column=1, sticky="ns", padx=5)
gen_button = tk.Button(gen_button_fr, text="Generate ", command=lambda: make_poem())
gen_button.grid(row=6, column=1, sticky="ns", padx=5)

# Save button
fr_buttons = tk.Frame(window)
fr_buttons.grid(row=15, column=1, sticky="sn", padx=5)
btn_save = tk.Button(fr_buttons, text="Save As File ", command=save_file)
btn_save.grid(row=15, column=1, sticky="s", padx=5)

resposive()
window.mainloop()